# App movie rest Backend



## Getting started

![image info 1](poster.png)


Hi and welcome, this repository contains the source code for a php-based web app that shows how to consume and create
a modern and quick web app from a REST service from IDBM.

Available online at:

https://veltainc-php-movies.ylfoqu.easypanel.host/

Note: this corresponds just to the Backend side, if you are interested to implement frontend side,
I recommend you visit the following repo:

https://gitlab.com/training-velta/usb/html/app-movie-frontend, it is the front end deploy site:







Code is organized as follows:


A model file responsible for management all DB conections and queries.


1. An API folder containing all the queries to the IDMB rest api endpoints. It acts like a "model"


2. A folder name config containing a file for configuring API keys for consuming the service.


3. An index file acting as a controller for orchesting different request made from end-users. 
   We have adopted a strategy for resuming app resquest from header file.


